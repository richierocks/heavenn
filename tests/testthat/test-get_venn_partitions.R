test_that(
  "get_venn_partitions with a list of 0 el'ts works",
  {
    x <- list()
    expected <-   data.frame(
      ..set..     = character(),
      ..values..   = logical(),
      ..n..       = integer(),
        stringsAsFactors = FALSE
    )
    expect_warning(
      actual <- get_venn_partitions(x),
      "There are no sets to partition\\."
    )
    expect_equal(actual, expected)
  }
)

test_that(
  "get_venn_partitions with a list of 1 el't works",
  {
    x <- list(a = 1:5)
    expected <- data.frame(
      a = TRUE,
      ..set.. = "a",
      stringsAsFactors = FALSE
    )
    expected$..values.. <- list(1:5)
    expected$..n.. <- 5L
    actual <- get_venn_partitions(x)
    expect_equal(actual, expected)
  }
)

test_that(
  "get_venn_partitions with a list of 2 el'ts works",
  {
    x2 <- list(a = 1:5, b = 4:6)
    expected <- data.frame(
      a = c(TRUE, FALSE, TRUE),
      b = c(TRUE, TRUE, FALSE),
      ..set.. = c("a\u2229b", "(b)\u2216(a)", "(a)\u2216(b)"),
      stringsAsFactors = FALSE
    )
    expected$..values.. <- list(4:5, 6, 1:3)
    expected$..n.. <- c(2L, 1L, 3L)
    actual <- get_venn_partitions(x2)
    expect_equal(actual, expected)
  }
)

test_that(
  "get_venn_partitions with a list of 3 el'ts works",
  {
    x3 <- list(a = 1:5, b = 4:6, c = c(1, 6, 10))
    expected <- data.frame(
      a = c(TRUE, FALSE, TRUE, FALSE, TRUE, FALSE, TRUE),
      b = c(TRUE, TRUE, FALSE, FALSE, TRUE, TRUE, FALSE),
      c = c(TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE),
      ..set.. = c(
        "a\u2229b\u2229c",
        "(b\u2229c)\u2216(a)", "(a\u2229c)\u2216(b)", "(c)\u2216(a\u222ab)",
        "(a\u2229b)\u2216(c)", "(b)\u2216(a\u222ac)", "(a)\u2216(b\u222ac)"
      ),
      stringsAsFactors = FALSE
    )
    expected$..values.. <- list(integer(), 6, 1, 10, 4:5, integer(), 2:3)
    expected$..n.. <- c(0L, 1L, 1L, 1L, 2L, 0L, 2L)
    actual <- get_venn_partitions(x3)
    expect_equal(actual, expected)
  }
)
