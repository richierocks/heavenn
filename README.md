[![Project Status: Wip - Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/0.1.0/wip.svg)](http://www.repostatus.org/#wip)
[![Is the package on CRAN?](http://www.r-pkg.org/badges/version/heavenn)](http://www.r-pkg.org/pkg/heavenn)
[![SemaphoreCI Build Status](https://semaphoreci.com/api/v1/projects/3f17cd3a-82de-4a5b-93cb-5dda571eb472/650294/badge.svg)](https://semaphoreci.com/richierocks/heavenn)
[![AppVeyor Build Status](https://ci.appveyor.com/api/projects/status/awob48b7sxn89uem?svg=true)](https://ci.appveyor.com/project/richierocks/heavenn)

# heavenn

Heavenly Venn diagrams.  There are at least six other R packages that can draw Venn diagrams.  Five of them are pretty awful, and *VennDiagram* is OK but weird and has a nasty codebase.  So here is package number seven(n), attempting to get it right this time.

*heavenn* aims to be easy to use, properly documented, and with a maintainable code base.  It isn't ready for real-world use yet, so stick with *VennDiagram* for now.

### Installation

To install the package, you first need the
[*devtools*](https://github.com/hadley/devtools) package.

```{r}
install.packages("devtools")
```

Then you can install the *heavenn* package using

```{r}
library(devtools)
install_bitbucket("richierocks/heavenn")
```

# Functionality

`venn` draws a Venn diagram.  You can provide the input as a list or data frame.  Currently only 1 or 2 sets are supported. Yeah.

`get_venn_partitions` returns a data frame with the counts that go in each set partition.
